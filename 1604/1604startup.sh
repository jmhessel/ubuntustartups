#!/bin/bash

sudo add-apt-repository ppa:deadsnakes/ppa
sudo apt-get update
sudo apt-get install python3.6
update-alternatives --install /usr/bin/python python /usr/bin/python3.6 1

sudo apt install htop texlive-latex-base texlive-latex-extra texlive-full curl python3-distutils

curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
sudo python get-pip.py

sudo pip install numpy scipy scikit-learn tqdm spacy gensim nltk transformers tensorflow h5py

sudo nautilus -q
sudo apt remove unity-lens-shopping
sudo pip install cython
sudo cat 1604bashrc >> ~/.bashrc
git config --global user.email "jmhessel@gmail.com"
git config --global user.name "Jack Hessel"
git config --global push.default matching

sudo mkdir ~/.extra_emacs
wget http://download.savannah.nongnu.org/releases/color-theme/color-theme-6.6.0.tar.gz
tar -xvf color-theme-6.6.0.tar.gz
sudo mv color-theme-6.6.0 ~/.extra_emacs
rm -rf color-theme*

cp my_dot_emacs.txt ~/.emacs
