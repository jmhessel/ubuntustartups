(add-to-list 'load-path "~/.extra_emacs/color-theme-6.6.0")

(setq-default c-basic-offset 4)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(inhibit-startup-screen t))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(defvar bjk-timestamp-format "%%JMH %Y-%m-%d: "
  "Format of date to insert with `bjk-timestamp' function
%Y-%m-%d %H:%M will produce something of the form YYYY-MM-DD HH:MM
Do C-h f on `format-time-string' for more info")


(defun dai ()
  "Insert a timestamp at the current point.
Note no attempt to go to beginning of line and no added carriage return.
Uses `bjk-timestamp-format' for formatting the date/time."
       (interactive)
       (insert (format-time-string bjk-timestamp-format (current-time)))
       )

(add-hook 'python-mode-hook '(lambda ()
  (local-set-key (kbd "RET") 'newline-and-indent)))

(add-hook 'bibtex-mode-hook (lambda () (electric-indent-local-mode -1)))

(require 'color-theme)
(color-theme-initialize)
(color-theme-robin-hood)

(add-hook 'java-mode-hook (lambda ()
			    (setq c-basic-offset 4
				  tab-width 4
				  indent-tabs-mode t)))
